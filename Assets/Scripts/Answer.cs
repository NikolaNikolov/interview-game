﻿using System.Collections;
using UnityEngine;

public enum Answer
{
    NotDividedByThreeAndFive = 0,
    DividedByThree = 1,
    DividedByFive = 2,
    DividedByThreeOrFive = DividedByThree | DividedByFive,
}
