﻿using System.Collections;
using UnityEngine;

public class Book : MonoBehaviour
{
    private const string MaterialMainTexture = "_MainTex";

    [SerializeField]
    private Material leftPage;

    [SerializeField]
    private Material rightPage;

    [SerializeField]
    private Material animationPageFace;

    [SerializeField]
    private Material animationPageBack;

    [SerializeField]
    private Texture defaultPageTexture;

    private string turnPageAnimationName;

    /// <summary>
    /// Change the content of the book's pages by switching their textures and triggering its turn page animation.
    /// </summary>
    /// <param name="newChapter"></param>
    public void ChangeChapter(BookChapterTextures newChapter)
    {
        // If a page is currently being turned, dalay the next changing of content until it finishes.
        if (this.animation.isPlaying)
        {
            AnimationState turnPageAnimationState = this.animation[this.turnPageAnimationName];
            float animationTimeLeft = turnPageAnimationState.length - turnPageAnimationState.time;
            StartCoroutine(ChangeChapterDelayed(newChapter, animationTimeLeft));
            return;
        }

        // Move the right page texture to the animation pages.
        Texture oldRightPageTexture = this.rightPage.GetTexture(MaterialMainTexture);
        this.animationPageFace.SetTexture(MaterialMainTexture, oldRightPageTexture);

        // Display the new content at the right page.
        this.animationPageBack.SetTexture(MaterialMainTexture, newChapter.LeftPageTexture);
        this.rightPage.SetTexture(MaterialMainTexture, newChapter.RightPageTexture);

        this.animation.Play();
    }

    private IEnumerator ChangeChapterDelayed(BookChapterTextures newChapter, float delay)
    {
        yield return new WaitForSeconds(delay);
        ChangeChapter(newChapter);
    }

    private void TurnPageEndHandler()
    {
        // At the end of the turn page animation, change the content of the left book page by switching the texture.
        Texture newLeftPageTexture = this.animationPageBack.GetTexture(MaterialMainTexture);
        this.leftPage.SetTexture(MaterialMainTexture, newLeftPageTexture);
    }

    private void Awake()
    {
        this.turnPageAnimationName = this.animation.clip.name;
    }

    private void OnDisable()
    {
        this.leftPage.SetTexture(MaterialMainTexture, this.defaultPageTexture);
        this.rightPage.SetTexture(MaterialMainTexture, this.defaultPageTexture);
        this.animationPageFace.SetTexture(MaterialMainTexture, this.defaultPageTexture);
        this.animationPageBack.SetTexture(MaterialMainTexture, this.defaultPageTexture);
    }
}
