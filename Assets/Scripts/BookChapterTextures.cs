﻿using System;
using System.Collections;
using UnityEngine;

[Serializable]
public class BookChapterTextures
{
    [SerializeField]
    private Texture leftPageTexture;

    [SerializeField]
    private Texture rightPageTexture;

    public Texture LeftPageTexture
    {
        get
        {
            return this.leftPageTexture;
        }
    }

    public Texture RightPageTexture
    {
        get
        {
            return this.rightPageTexture;
        }
    }
}
