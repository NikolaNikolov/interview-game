﻿using System;
using System.Collections;
using UnityEngine;

public class Button3D : MonoBehaviour
{
    [SerializeField]
    private Answer answer;

    public event Action<Answer> Pressed;

    public Answer Answer
    {
        get
        {
            return this.answer;
        }
    }

    // Basic touch/click detection for simplicity's sake.
    private void OnMouseDown()
    {
        if (Pressed != null)
        {
            Pressed(answer);
        }
    }
}
