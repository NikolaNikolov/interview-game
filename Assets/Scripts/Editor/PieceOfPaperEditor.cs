﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(PieceOfPaper))]
public class PieceOfPaperEditor : Editor
{
    private PieceOfPaper currentPieceOfPaper;

    private void OnEnable()
    {
        this.currentPieceOfPaper = this.target as PieceOfPaper;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Save current as shown position and rotation"))
        {
            this.currentPieceOfPaper.SaveAsShownPositionAndRotation();
            EditorUtility.SetDirty(this.currentPieceOfPaper);
        }

        if (GUILayout.Button("Save current as hidden position and rotation"))
        {
            this.currentPieceOfPaper.SaveAsHiddenPositionAndRotation();
            EditorUtility.SetDirty(this.currentPieceOfPaper);
        }

        if (GUILayout.Button("Move to shown position and rotation"))
        {
            this.currentPieceOfPaper.MoveToShownPositionAndRotation();
        }

        if (GUILayout.Button("Move to hidden position and rotation"))
        {
            this.currentPieceOfPaper.MoveToHiddenPositionAndRotation();
        }
    }
}
