﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private int minQuestionNumber = 1;
    [SerializeField]
    private int maxQuestionNumber = 1000;

    [SerializeField]
    private float timeBetweenGames = 3f;

    [SerializeField]
    private int targetFramerate = 60;

    [SerializeField]
    private Book book;

    [SerializeField]
    private BookChapterTextures howToPlayChapter;
    [SerializeField]
    private BookChapterTextures[] countdownChapters;
    [SerializeField]
    private BookChapterTextures correctAnswerChapter;
    [SerializeField]
    private BookChapterTextures gameOverChapter;

    [SerializeField]
    private PieceOfPaper currentStreak;
    [SerializeField]
    private PieceOfPaper question;

    [SerializeField]
    private PieceOfPaper dividedByThreePaper;
    [SerializeField]
    private PieceOfPaper dividedByFivePaper;
    [SerializeField]
    private PieceOfPaper dividedByThreeAndFivePaper;
    [SerializeField]
    private PieceOfPaper notDividedByThreeOrFive;

    [SerializeField]
    private Material buttonsGlowMaterial;

    private Color buttonsGlowMaterialColor;

    private Answer playerAnswer;
    private Answer correctAnswer;

    private Button3D dividedByThree;
    private Button3D dividedByFive;
    private Button3D dividedByThreeAndFive;

    private int currentStreakCount;

    private static GameController Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        this.dividedByThree = this.dividedByThreePaper.GetComponent<Button3D>();
        this.dividedByFive = this.dividedByFivePaper.GetComponent<Button3D>();
        this.dividedByThreeAndFive = this.dividedByThreeAndFivePaper.GetComponent<Button3D>();

        Application.targetFrameRate = targetFramerate;

        this.buttonsGlowMaterialColor = this.buttonsGlowMaterial.color;
        DisableButtonsGlow();
    }

    private void Start()
    {
        OpenHowToPlayChapter();
    }

    private void OpenHowToPlayChapter()
    {
        this.currentStreakCount = 0;
        this.currentStreak.Hide();

        this.book.ChangeChapter(this.howToPlayChapter);
        StartCoroutine(StartNewGameAfterPlayerInput());
    }

    /// <summary>
    /// Wait for the player to touch the screen before starting a new game.
    /// </summary>
    private IEnumerator StartNewGameAfterPlayerInput()
    {
        // Wait for any animations to complete before starting a new game.
        yield return new WaitForSeconds(PieceOfPaper.AnimationDuration);

        while (true)
        {
            // Basic touch/click detection for simplicity's sake.
            if (Input.GetMouseButtonDown(0))
            {
                StartNewGame();
                break;
            }

            yield return null;
        }
    }

    private void StartNewGame()
    {
        StartCoroutine("GameplayCoroutine");
    }

    private IEnumerator GameplayCoroutine()
    {
        int currentCountdownChapter = 0;
        int timeLeftToAnswer = this.countdownChapters.Length - 1;

        this.book.ChangeChapter(this.countdownChapters[currentCountdownChapter]);
        DisplayNewQuestion();

        // Wait for the animations to complete before the countdown timer starts.
        yield return new WaitForSeconds(PieceOfPaper.AnimationDuration);

        // Reset the answer of the player to its default value.
        this.playerAnswer = Answer.NotDividedByThreeAndFive;
        this.dividedByThree.Pressed += SetPlayerAnswer;
        this.dividedByFive.Pressed += SetPlayerAnswer;
        this.dividedByThreeAndFive.Pressed += SetPlayerAnswer;

        // Show the player that he or she can iteract with the buttons.
        EnableButtonsGlow();

        // Turn a new page of the book each second, while the countdown timer is running.
        while (timeLeftToAnswer > 0)
        {
            yield return new WaitForSeconds(1f);

            timeLeftToAnswer--;
            currentCountdownChapter++;

            this.book.ChangeChapter(this.countdownChapters[currentCountdownChapter]);
        }

        // This point is only reached if the time runs out.
        yield return new WaitForSeconds(1f);
        CompareAnswer();
    }

    /// <summary>
    /// If the player selects an answer, stop the gameplay coroutine and check if it is correct.
    /// </summary>
    /// <param name="answer">The answer given by the player.</param>
    private void SetPlayerAnswer(Answer answer)
    {
        StopCoroutine("GameplayCoroutine");
        this.playerAnswer = answer;
        CompareAnswer();
    }

    /// <summary>
    /// Compare the answer of the player with the correct answer and either return to the how to play
    /// screen (in case of an incorrect answer) or start a new round (in case of a correct answer).
    /// </summary>
    private void CompareAnswer()
    {
        DisableButtonsGlow();

        this.dividedByThree.Pressed -= SetPlayerAnswer;
        this.dividedByFive.Pressed -= SetPlayerAnswer;
        this.dividedByThreeAndFive.Pressed -= SetPlayerAnswer;

        if (this.playerAnswer == this.correctAnswer)
        {
            StartCoroutine(UpdateCurrentStreak());

            this.book.ChangeChapter(this.correctAnswerChapter);
            Invoke("StartNewGame", this.timeBetweenGames);
            HideQuestionAndAnswers();
        }
        else
        {
            this.book.ChangeChapter(this.gameOverChapter);
            Invoke("OpenHowToPlayChapter", this.timeBetweenGames);
            Invoke("HideQuestionAndAnswers", this.timeBetweenGames);

            // Hide all the wrong answers and leave out only the correct one, for the player to see.
            if (this.correctAnswer == Answer.NotDividedByThreeAndFive)
            {
                this.notDividedByThreeOrFive.Show();
            }

            if (this.dividedByThree.Answer != this.correctAnswer)
            {
                this.dividedByThreePaper.Hide();
            }

            if (this.dividedByFive.Answer != this.correctAnswer)
            {
                this.dividedByFivePaper.Hide();
            }

            if (this.dividedByThreeAndFive.Answer != this.correctAnswer)
            {
                this.dividedByThreeAndFivePaper.Hide();
            }
        }
    }

    /// <summary>
    /// Roll a new number for the question, calculate the correct answer and display the question and answers to the player.
    /// </summary>
    private void DisplayNewQuestion()
    {
        int questionNumber = Random.Range(this.minQuestionNumber, this.maxQuestionNumber + 1);
        this.correctAnswer = Answer.NotDividedByThreeAndFive;
        if (questionNumber % 3 == 0)
        {
            this.correctAnswer |= Answer.DividedByThree;
        }

        if (questionNumber % 5 == 0)
        {
            this.correctAnswer |= Answer.DividedByFive;
        }

        this.question.Show(questionNumber.ToString());
        this.dividedByThreePaper.Show();
        this.dividedByFivePaper.Show();
        this.dividedByThreeAndFivePaper.Show();
    }

    private void HideQuestionAndAnswers()
    {
        this.question.Hide();
        this.dividedByThreePaper.Hide();
        this.dividedByFivePaper.Hide();
        this.dividedByThreeAndFivePaper.Hide();
        this.notDividedByThreeOrFive.Hide();
    }

    /// <summary>
    /// Hide the piece of paper that holds the current streak, update its value and show it again.
    /// </summary>
    private IEnumerator UpdateCurrentStreak()
    {
        this.currentStreak.Hide();

        yield return new WaitForSeconds(PieceOfPaper.AnimationDuration);

        this.currentStreakCount++;
        this.currentStreak.Show(this.currentStreakCount.ToString());
    }

    private void EnableButtonsGlow()
    {
        this.buttonsGlowMaterial.color = this.buttonsGlowMaterialColor;
    }

    private void DisableButtonsGlow()
    {
        this.buttonsGlowMaterial.color = Color.black;
    }

    private void OnDisable()
    {
        EnableButtonsGlow();
    }
}
