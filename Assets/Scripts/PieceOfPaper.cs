﻿using UnityEngine;
using System.Collections;
using System;

public class PieceOfPaper : MonoBehaviour
{
    public const float AnimationDuration = 0.2f;

    // The position and rotation of the piece of paper, when it is displayed to the player.
    [SerializeField]
    private Vector3 shownPosition;
    [SerializeField]
    private Quaternion shownRotation;

    // The position and rotation of the piece of paper, when it is hidden from sight.
    [SerializeField]
    private Vector3 hiddenPosition;
    [SerializeField]
    private Quaternion hiddenRotation;

    [SerializeField]
    private TextMesh dynamicContent;

    private MeshRenderer[] meshRenderers;
    private MeshCollider meshCollider;

    /// <summary>
    /// Save the current position and rotation of the object as its shown
    /// position and rotation (meant for usage in the editor only).
    /// </summary>
    public void SaveAsShownPositionAndRotation()
    {
        this.shownPosition = this.transform.position;
        this.shownRotation = this.transform.rotation;
    }

    /// <summary>
    /// Save the current position and rotation of the object as its hidden
    /// position and rotation (meant for usage in the editor only).
    /// </summary>
    public void SaveAsHiddenPositionAndRotation()
    {
        this.hiddenPosition = this.transform.position;
        this.hiddenRotation = this.transform.rotation;
    }

    /// <summary>
    /// Move the object to its the current shown position and rotation (meant for usage in the editor only).
    /// </summary>
    public void MoveToShownPositionAndRotation()
    {
        this.transform.position = this.shownPosition;
        this.transform.rotation = this.shownRotation;
    }

    /// <summary>
    /// Move the object to its the current hidden position and rotation (meant for usage in the editor only).
    /// </summary>
    public void MoveToHiddenPositionAndRotation()
    {
        this.transform.position = this.hiddenPosition;
        this.transform.rotation = this.hiddenRotation;
    }

    /// <summary>
    /// Enable the piece of paper's renderers and collider, update its text
    /// and animate it to its predefined shown position and rotation.
    /// </summary>
    /// <param name="newText">The new text to be displayed.</param>
    public void Show(string newText = null)
    {
        if (newText != null && this.dynamicContent != null)
        {
            this.dynamicContent.text = newText;
        }

        EnableInteractivity();
        StartCoroutine(Animate(this.hiddenPosition, this.shownPosition, this.hiddenRotation, this.shownRotation));
    }

    /// <summary>
    /// Animate the piece of paper to its predefined shown position and rotation,
    /// and disable its renderers and collider after the animation completes.
    /// </summary>
    public void Hide()
    {
        StartCoroutine(Animate(this.shownPosition, this.hiddenPosition, this.shownRotation, this.hiddenRotation, DisableInteractivity));
    }

    private void Awake()
    {
        this.meshRenderers = GetComponentsInChildren<MeshRenderer>();
        this.meshCollider = GetComponentInChildren<MeshCollider>();

        DisableInteractivity();
    }

    private IEnumerator Animate(Vector3 fromPosition, Vector3 toPosition, Quaternion fromRotation, Quaternion toRotation, Action callback = null)
    {
        float animationTime = 0f;
        while (animationTime < 1f)
        {
            animationTime += Time.deltaTime / AnimationDuration;

            this.transform.position = Vector3.Lerp(fromPosition, toPosition, animationTime);
            this.transform.rotation = Quaternion.Slerp(fromRotation, toRotation, animationTime);

            yield return null;
        }

        if (callback != null)
        {
            callback();
        }
    }

    private void EnableInteractivity()
    {
        this.transform.position = this.hiddenPosition;
        this.transform.rotation = this.hiddenRotation;

        foreach (MeshRenderer meshRenderer in this.meshRenderers)
        {
            meshRenderer.enabled = true;
        }

        if (this.meshCollider != null)
        {
            this.meshCollider.enabled = true;
        }
    }

    private void DisableInteractivity()
    {
        foreach (MeshRenderer meshRenderer in this.meshRenderers)
        {
            meshRenderer.enabled = false;
        }

        if (this.meshCollider != null)
        {
            this.meshCollider.enabled = false;
        }
    }
}
